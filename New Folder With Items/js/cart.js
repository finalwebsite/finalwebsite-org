/*---------Cart----------*/
const cartOpen = document.querySelector('.cart');
const cartClose = document.querySelector('.close_cart');
const cartmenu = document.querySelector('.nav_menu_cart');
const cartContainer = document.querySelector('.nav_menu_cart');

cartOpen.addEventListener('click', () => {
    cartmenu.classList.add("open");
    cartContainer.style.right = "0rem";
    cartContainer.style.width = "40rem";
})

cartClose.addEventListener('click', () => {
    cartmenu.classList.remove("open");
    cartContainer.style.right = "-40rem";
    cartContainer.style.width = "0";
})